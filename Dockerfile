FROM debian:10

RUN apt-get update && apt-get install -y wget python3 sqlite3 && mkdir looklikematrix && cd looklikematrix
WORKDIR /lookslikematrix
RUN wget https://raw.githubusercontent.com/haiwen/seafile-rpi/master/build3.sh
RUN chmod u+x build3.sh
RUN ./build3.sh 8.0.5